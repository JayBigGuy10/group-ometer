import time
import os
import RPi.GPIO as GPIO
import smbus
import glob
import shutil
import os.path

n2d = 0
cpdir = 0
chdir = 0
I2C_ADDR  = 0x3f 
LCD_WIDTH = 16 
LCD_CHR = 1
LCD_CMD = 0
LCD_LINE_1 = 0x80 
LCD_LINE_2 = 0xC0 
LCD_LINE_3 = 0x94 
LCD_LINE_4 = 0xD4
LCD_BACKLIGHT  = 0x08  # On
#LCD_BACKLIGHT = 0x00  # Off
ENABLE = 0b00000100 # Enable bit
E_PULSE = 0.0005
E_DELAY = 0.0005
#bus = smbus.SMBus(0)  # Rev 1 Pi uses 0
bus = smbus.SMBus(1) # Rev 2 Pi uses 1
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def lcd_init():
  lcd_byte(0x33,LCD_CMD) 
  lcd_byte(0x32,LCD_CMD) 
  lcd_byte(0x06,LCD_CMD) 
  lcd_byte(0x0C,LCD_CMD) 
  lcd_byte(0x28,LCD_CMD) 
  lcd_byte(0x01,LCD_CMD) 
  time.sleep(E_DELAY)
def lcd_byte(bits, mode):
  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT
  bits_low = mode | ((bits<<4) & 0xF0) | LCD_BACKLIGHT
  bus.write_byte(I2C_ADDR, bits_high)
  lcd_toggle_enable(bits_high)
  bus.write_byte(I2C_ADDR, bits_low)
  lcd_toggle_enable(bits_low)
def lcd_toggle_enable(bits):
  time.sleep(E_DELAY)
  bus.write_byte(I2C_ADDR, (bits | ENABLE))
  time.sleep(E_PULSE)
  bus.write_byte(I2C_ADDR,(bits & ~ENABLE))
  time.sleep(E_DELAY)
def lcd_string(message,line):
  message = message.ljust(LCD_WIDTH," ")
  lcd_byte(line, LCD_CMD)
  for i in range(LCD_WIDTH):
    lcd_byte(ord(message[i]),LCD_CHR)
def main():
  lcd_init()
  while True:
    lcd_string("Created by a        <",LCD_LINE_1)
    lcd_string("Youtuber Called        <",LCD_LINE_2)
    time.sleep(3)
    lcd_string("youtube.com/c/ ",LCD_LINE_1)
    lcd_string("jaydenlitolff ",LCD_LINE_2)
    time.sleep(3)
    lcd_string("Bright Sparks ",LCD_LINE_1)
    lcd_string("at T.I.S NZ ",LCD_LINE_2)
    time.sleep(3)

lcd_init()
lcd_string("Starting",LCD_LINE_1)
lcd_string("JBGuy Tech 2017 ",LCD_LINE_2)
time.sleep(2.0)
lcd_string("Roll Call    [~]",LCD_LINE_1)
lcd_string("Re-Call      [ ]",LCD_LINE_2)
down = 0
countdown = 0
while True:
    brk = 0
    input_state17 = GPIO.input(17)
    if input_state17 == False:
        print('down')
        if countdown == 0:
            lcd_string("Roll Call    [ ]",LCD_LINE_1)
            lcd_string("Re-Call      [~]",LCD_LINE_2)
            down = 1
            time.sleep(0.5)
	if countdown == 1:
            lcd_string("Re-Call      [ ]",LCD_LINE_1)
            lcd_string("Clear Cache  [~]",LCD_LINE_2)
	    down = 2
	    time.sleep(0.5)
	if countdown == 2:
            lcd_string("Roll Call    [~]",LCD_LINE_1)
            lcd_string("Re-Call      [ ]",LCD_LINE_2)
            down = 0
            time.sleep(0.5)
	countdown = down
    input_state18 = GPIO.input(18)
    if input_state18 == False:
        print('in')
        if down == 0:
	    input_state17 = GPIO.input(17)
	    while input_state17 == True:
	        print('roll call')
                lcd_string("Waiting",LCD_LINE_1)
                lcd_string("Scan Barcode",LCD_LINE_2)
	        barin = raw_input("Barcode: ")
		if "#" not in barin:
		    lcd_string("Invalid",LCD_LINE_1)
		    lcd_string("Barcode",LCD_LINE_2)
		    time.sleep(3.0)
		    lcd_string("Roll Call    [~]",LCD_LINE_1)
		    lcd_string("Re-Call      [ ]",LCD_LINE_2)
		    break
		if barin == "":
		    break
	        f= open("/home/pi/groupometer/barstore/" + barin + ".txt","w+")
	        f.close
	        barsp = barin.split('#')
	        lcd_string(barsp[0],LCD_LINE_1)
                lcd_string(barsp[1],LCD_LINE_2)
	        t_end = time.time() + 3
	        while time.time() < t_end:
	            input_state17 = GPIO.input(17) 	    
		    if input_state17 == False:
		        break

        if down == 1:
	    input_state17 = GPIO.input(17)
	    while input_state17 == True:
	        if brk == 1:
		    brk = 0
		    break
		print('re call')
		if chdir == 0:
		    os.chdir('/home/pi/groupometer/barstore/')
		    chdir = 1
		
		if cpdir == 0:
		    cpdir = 1
		    for txt_file in glob.iglob('*.txt'):
    		        shutil.copy2(txt_file, '/home/pi/groupometer/temp/')

                lcd_string("Waiting",LCD_LINE_1)
                lcd_string("Scan Barcode",LCD_LINE_2)
	        barin = raw_input("Barcode: ")
		if barin == "":
		    break
		scannedfile = "/home/pi/groupometer/temp/" + barin + ".txt"
		if os.path.isfile(scannedfile) == True:
	            os.remove(scannedfile)
	            barsp = barin.split('#')
	            lcd_string(barsp[0],LCD_LINE_1)
                    lcd_string(barsp[1],LCD_LINE_2)
		else:
	            lcd_string("Invalid",LCD_LINE_1)
                    lcd_string("Person",LCD_LINE_2)
	        t_end = time.time() + 3
	        while time.time() < t_end:
	            input_state17 = GPIO.input(17) 	    
		    if input_state17 == False:
			dirf = os.listdir('/home/pi/groupometer/temp')
		        dirc = len(dirf)
		        lcd_string("People",LCD_LINE_1)
 		        lcd_string("Not Present",LCD_LINE_2)
			time.sleep(1.5)
			print('ok1')
			for f in dirf:
			    f = f[:-4]
			    dir = f.split('#')
		            lcd_string(dir[0],LCD_LINE_1)
 		            lcd_string(dir[1],LCD_LINE_2)
  		            time.sleep(3.0)
            		lcd_string("Roll Call    [ ]",LCD_LINE_1)
           		lcd_string("Re-Call      [~]",LCD_LINE_2)
			break

        if down == 2:
	    toclr1 = os.listdir('/home/pi/groupometer/temp')
	    toclr2 = os.listdir('/home/pi/groupometer/barstore')
	    for fc1 in toclr1:
	        os.remove('/home/pi/groupometer/temp/' + fc1)
	    print('temp clear')
	    for fc2 in toclr2:
		os.remove('/home/pi/groupometer/barstore/' + fc2)
	    print('barstore clear')
	    lcd_string('Cache',LCD_LINE_1)
 	    lcd_string('Clear',LCD_LINE_2)
	    time.sleep(3.0)
	    lcd_string('Re-Call      [ ]',LCD_LINE_1)
 	    lcd_string('Clear Cache  [~]',LCD_LINE_2)

